let creditInputField = document.getElementById("creditInputField");
let procentInputField = document.getElementById("procentInputField");
let monthInputField = document.getElementById("monthInputField");
let repaymentInputField = document.getElementById("repaymentInputField");

let sumOfPaymentOutputField = document.getElementById("sumOfPaymentOutputField");
let overpaymentOutputField = document.getElementById("overpaymentOutputField");

let monthlyPayment;

function Calculate()
{
    let payment = 0;
    if (repaymentInputField.options[repaymentInputField.selectedIndex].value == "differentiated")
    {
        let credit = creditInputField.value;
        let month = monthInputField.value;
        for (let i = 0; i < monthInputField.value; i++) 
        {
            monthlyPayment = (creditInputField.value/monthInputField.value) + ((credit*procentInputField.value)/(100*12));
            credit -= creditInputField.value/monthInputField.value;
            month -= 1;
            payment += monthlyPayment;
        }        
    }
    else
    {
        let procentsInMounth = procentInputField.value/100/12;
        monthlyPayment = creditInputField.value * (procentsInMounth+(procentsInMounth/((Math.pow(1+procentsInMounth,monthInputField.value))-1)));
        payment = (monthlyPayment*monthInputField.value).toFixed(2);
    } 
    sumOfPaymentOutputField.innerHTML = "Всего выплат:"+payment;
    overpaymentOutputField.innerHTML = "Переплата:"+(payment-creditInputField.value);
}


function ClearField()
{
    creditInputField.value ="";
    procentInputField.value ="";
    monthInputField.value ="";

    sumOfPaymentOutputField.innerHTML ="";
    overpaymentOutputField.innerHTML ="";
}
let playerNameInputField = document.getElementById("playerNameInputField");
let playerScoreInputField = document.getElementById("playerScoreInputField");

let playerTableField = document.getElementById("playerTableField");

let playersNames = [];
let playersScores = [];

function RenderPlayers()
{
    let htmlCode="<table border='1'>";
    htmlCode+="<tr>";
    htmlCode+="<th>Имя игрока</th>";
    htmlCode+="<th>Счет игрока</th>";   
    htmlCode+="</tr>";

    for(let i=0; i<playersNames.length;i++)
    {
        htmlCode+="<tr>";
        htmlCode+="<td>"+playersNames[i]+"</td>";
        htmlCode+="<td>"+playersScores[i]+"</td>";
        htmlCode+="</tr>";
    }
    htmlCode+="</table>";

    playerTableField.innerHTML = htmlCode;


}

function AddPlayer()
{
    let playerName = playerNameInputField.value;
    playersNames.push(playerName);
    let playerScore = playerScoreInputField.value;
    playersScores.push(playerScore);

    RenderPlayers();

    playerNameInputField.value="";
    playerScoreInputField.value="";
}
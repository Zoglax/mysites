let difSelectField = document.getElementById("difSelectField");

let customDifContainer = document.getElementById("customDifContainer");
let customRightSideInputField = document.getElementById("customRightSideInputField");
let triesCountInputField = document.getElementById("triesCountInputField");

let userNumberInputField = document.getElementById("userNumberInputField");
let compAnswerInputField = document.getElementById("compAnswerInputField");
let compTextInputField = document.getElementById("compTextInputField");
let compTipInputField = document.getElementById("compTipInputField");

let triesCountOutputField = document.getElementById("triesCountOutputField");

let buttonCheck = document.getElementById("buttonCheck");
let buttonStart = document.getElementById("buttonStart");
let buttonRestart = document.getElementById("buttonRestart");

let triesCount;
let rightSide;
let leftSide;
let userNumber;
let compNumber;
let difLevel;

let looseDivContainer = document.getElementById("looseDivContainer");
let easterEgg7DivContainer = document.getElementById("easterEgg7DivContainer");
let easterEgg9DivContainer = document.getElementById("easterEgg9DivContainer");
let easterEgg66DivContainer = document.getElementById("easterEgg66DivContainer");
let easterEgg96DivContainer = document.getElementById("easterEgg96DivContainer");
let easterEgg33DivContainer = document.getElementById("easterEgg33DivContainer");
let easterEgg666DivContainer = document.getElementById("easterEgg666DivContainer");
let easterEgg777DivContainer = document.getElementById("easterEgg777DivContainer");
let criticalTriesCount = document.getElementById("criticalTriesCount");

buttonCheck.disabled=true;
buttonStart.disabled=false;
buttonRestart.disabled=true;


function OnDifChange()
{
    difLevel = difSelectField.options
    [difSelectField.selectedIndex].text;

    if(difLevel == "CUSTOM")
    {
        customDifContainer.classList.remove("hidden-div");
    }
    else
    {
        customDifContainer.classList.add("hidden-div");
    }
}

function GameStart()
{
    buttonCheck.disabled=false;
    buttonStart.disabled=true;
    let difLevel = difSelectField.options
    [difSelectField.selectedIndex].text;
    if (difLevel=="EASY")
    {
        triesCount=50;
        rightSide=100;
    }
    else if (difLevel=="MEDIUM")
    {
        triesCount=25;
        rightSide=300;
    }
    else if (difLevel=="HARD")
    {
        triesCount=10;
        rightSide=500;
    }
    else if (difLevel=="CUSTOM")
    {
        triesCount=parseInt(triesCountInputField.value);
        rightSide=parseInt(customRightSideInputField.value);
    }
    compNumber=Math.round(Math.random()*rightSide)+1;

    triesCountOutputField.innerHTML="count left tries:"+triesCount;

    customDifContainer.classList.add("hidden-div");

    leftSide=1;

    alert("i guessed number from 1 to "+rightSide);
}

function CheckNumber()
{
    userNumber=parseInt(userNumberInputField.value);
    triesCount--;
    let isWin=false;
    if (userNumber>compNumber)
    {
        compAnswerInputField.innerHTML="input smaller";
        rightSide=userNumberInputField.value;
        compTipInputField.innerHTML="tip: interval from "+leftSide+" to "+rightSide;
        userNumberInputField.value="";
    }
    else if (userNumber<compNumber)
    {
        compAnswerInputField.innerHTML="input bigger";
        leftSide=userNumberInputField.value;
        compTipInputField.innerHTML="tip: interval from "+leftSide+" to "+rightSide;
        userNumberInputField.value="";
    }
    else
    {
        compAnswerInputField.innerHTML="you are guess";
        userNumberInputField.value="";
        isWin=true;
        buttonRestart.disabled=false;
        buttonCheck.disabled=true;

        difLevel = difSelectField.options
        [difSelectField.selectedIndex].text;

        if(difLevel == "EASY")
        {
            if(triesCount<50 && triesCount>=45)
            {
                compTextInputField.innerHTML="Нихя ты баклажан, блятб.";
            }
            else if (triesCount<45 && triesCount>=40)
            {  
                compTextInputField.innerHTML="Не в натуре класс, збс, четко.";
            }
            else if (triesCount<40 && triesCount>=35)
            {  
                compTextInputField.innerHTML="Ни че так.";
            }
            else if (triesCount<35 && triesCount>=30)
            {  
                compTextInputField.innerHTML="Ну, норм.";
            }
            else if (triesCount<30 && triesCount>=25)
            {  
                compTextInputField.innerHTML="Ну, норм, вроде как.";
            }
            else if (triesCount<25 && triesCount>=20)
            {  
                compTextInputField.innerHTML="Такое себе.";
            }
            else if (triesCount<20 && triesCount>=15)
            {  
                compTextInputField.innerHTML="Либо компуктер тя троллит, либо ты ебанулся))).";
            }
            else if (triesCount<15 && triesCount>=10)
            {  
                compTextInputField.innerHTML="Чет слабовато.";
            }
            else if (triesCount<10 && triesCount>=5)
            {  
                compTextInputField.innerHTML="Ваще херово.";
            }
            else if (triesCount<5 && triesCount>=1)
            {  
                compTextInputField.innerHTML="Ваще пздц нхй блтб.";
                criticalTriesCount.classList.remove("hidden-critical-tries");
            }
        }
        else if(difLevel == "MEDIUM")
        {
            if(triesCount<25 && triesCount>=20)
            {
                compTextInputField.innerHTML="Нихя ты баклажан, блятб.";
            }
            else if (triesCount<20 && triesCount>=15)
            {  
                compTextInputField.innerHTML="Не в натуре класс, збс, четко.";
            }
            else if (triesCount<15 && triesCount>=10)
            {  
                compTextInputField.innerHTML="Ну, норм, вроде как.";
            }
            else if (triesCount<10 && triesCount>=5)
            {  
                compTextInputField.innerHTML="Либо компуктер тя троллит, либо ты ебанулся))).";
            }
            else if (triesCount<5 && triesCount>=1)
            {  
                compTextInputField.innerHTML="Ваще пздц нхй блтб.";
                criticalTriesCount.classList.remove("hidden-critical-tries");
            }
        }
        
        else if(difLevel == "HARD")
        {
            if(triesCount<10 && triesCount>=8)
            {
                compTextInputField.innerHTML="Нихя ты баклажан, блятб.";
            }
            if(triesCount<8 && triesCount>=6)
            {
                compTextInputField.innerHTML="Либо компуктер тя троллит, либо ты ебанулся))).";
            }
            if(triesCount<4 && triesCount>=1)
            {
                compTextInputField.innerHTML="Ваще пздц нхй блтб.";
                criticalTriesCount.classList.remove("hidden-critical-tries");
            } 
        }
    }


    if(userNumber==7)
    {
        easterEgg7DivContainer.classList.remove("hidden-div-easter-egg7");
    }
    else
    {
        easterEgg7DivContainer.classList.add("hidden-div-easter-egg7");
    }
    if(userNumber==9)
    {
        easterEgg9DivContainer.classList.remove("hidden-div-easter-egg9");
    }
    else
    {
        easterEgg9DivContainer.classList.add("hidden-div-easter-egg9");
    }
    if(userNumber==66)
    {
        easterEgg66DivContainer.classList.remove("hidden-div-easter-egg66");
    }
    else
    {
        easterEgg66DivContainer.classList.add("hidden-div-easter-egg66");
    }
    if(userNumber==96)
    {
        easterEgg96DivContainer.classList.remove("hidden-div-easter-egg96");
    }
    else
    {
        easterEgg96DivContainer.classList.add("hidden-div-easter-egg96");
    }
    if(userNumber==33)
    {
        easterEgg33DivContainer.classList.remove("hidden-div-easter-egg33");
    }
    else
    {
        easterEgg33DivContainer.classList.add("hidden-div-easter-egg33");
    }
    if(userNumber==666)
    {
        easterEgg666DivContainer.classList.remove("hidden-div-easter-egg666");
    }
    else
    {
        easterEgg666DivContainer.classList.add("hidden-div-easter-egg666");
    }
    if(userNumber==777)
    {
        easterEgg777DivContainer.classList.remove("hidden-div-easter-egg777");
    }
    else
    {
        easterEgg777DivContainer.classList.add("hidden-div-easter-egg777");
    }
    
    if (triesCount==0)
    {
        if (isWin==false)
        {
            buttonCheck.disabled=true;
            buttonRestart.disabled=false;
            alert("Ou maewa mo shinderu");
            looseDivContainer.classList.remove("hidden-div-loose");
        }
        else
        {
            looseDivContainer.classList.add("hidden-div-loose");
        }
    }
    else
    {
        triesCountOutputField.innerHTML="count left tries:"+triesCount;
    }
}
function RestartGame()
{
    buttonStart.disabled=false;

    triesCountOutputField.innerHTML="";
    compAnswerInputField.innerHTML="";
    compTipInputField.innerHTML="";
    compTextInputField.innerHTML="";

    looseDivContainer.classList.add("hidden-div-loose");
    criticalTriesCount.classList.add("hidden-critical-tries");
    easterEgg7DivContainer.classList.add("hidden-div-easter-egg7");
    easterEgg9DivContainer.classList.add("hidden-div-easter-egg9");
    easterEgg66DivContainer.classList.add("hidden-div-easter-egg66");
    easterEgg96DivContainer.classList.add("hidden-div-easter-egg96");
    easterEgg33DivContainer.classList.add("hidden-div-easter-egg33");
    easterEgg666DivContainer.classList.add("hidden-div-easter-egg666");
    easterEgg777DivContainer.classList.add("hidden-div-easter-egg777");

    buttonRestart.disabled=true;
}

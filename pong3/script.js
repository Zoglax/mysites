let playArea = document.getElementById("playArea");

let rocket1X,rocket1Y;
let rocket2X,rocket2Y;
let rocketH,rocketW;

let rocketDY;

let ballX,ballY;
let ballHW;
let ballDX,ballDY;

let scoreLeft, scoreRight;

let keysPressed;

let context;

let KeyUp1 = "w", KeyDown1 = "s";
let KeyUp2 = "ArrowUp", KeyDown2 = "ArrowDown";
 
function KeyDownHadler(e){
    if(e.key==KeyUp1 ||e.key==KeyDown1 ||e.key==KeyUp2 ||e.key==KeyDown2)
    {
        keysPressed [e.key] =true;

    }   
}

function KeyUpHadler(e){
    if(e.key==KeyUp1 ||e.key==KeyDown1 ||e.key==KeyUp2 ||e.key==KeyDown2)
    {
        keysPressed [e.key] =false;

    }   
}
function IntialValues()
{
    context =playArea.getContext("2d");
    scaleFactor = Math.sqrt(Math.pow(playArea.clientWidth,2)+Math.pow(playArea.height,2));

    rocketH = Math.floor(scaleFactor/5);
    rocketW = Math.floor(rocketH/8);
    rocket1X = 0;
    rocket1Y = playArea.height/2-rocketH/2;

    rocket2X = playArea.width-rocketW;
    rocket2Y = rocket1Y;

    rocketDY =Math.floor(rocketH/20);

    ballHW = rocketW;
    ballX = Math.floor(playArea.width/2-ballHW/2);
    ballY = Math.floor(playArea.height/2-ballHW/2);

    ballDX =1;
    ballDY =1;

    keysPressed =[];

    document.addEventListener("keydown" , KeyDownHadler , false);
    document.addEventListener("keyup" , KeyUpHadler , false);
}

function RenderObjects()
{
    context.clearRect(0,0, playArea.width,playArea.height);

    context.fillStyle ="darkcyan";
    context.fillRect(rocket1X,rocket1Y,rocketW,rocketH);
    context.fillRect(rocket2X,rocket2Y,rocketW,rocketH);
    context.fillRect(ballX,ballY,ballHW,ballHW);
}

function CalculatePhysics()
{
   if(keysPressed[KeyUp1]==true)
   {
       rocket1Y-=rocketDY;
        if(rocket1Y<0)
        {
            rocket1Y = 0;
        }
   }
   if(keysPressed[KeyDown1]==true)
   {
       rocket1Y+=rocketDY;
        if(rocket1Y+rocketH>playArea.height)
        {
            rocket1Y = playArea.height-rocketH;
        }
   }

   if(keysPressed[KeyUp2]==true)
   {
       rocket2Y-=rocketDY;
        if(rocket2Y<0)
        {
            rocket2Y = 0;
        }
   }
   if(keysPressed[KeyDown2]==true)
   {
       rocket2Y+=rocketDY;
        if(rocket2Y+rocketH>playArea.height)
        {
            rocket2Y = playArea.height-rocketH;
        }
   }
    ballX+=ballDX;
    ballY+=ballDY;

    if(ballY+ballHW>=playArea.height || ballY<=0)
    {
        ballDY=-ballDY;
    }
    if(ballX+ballHW>=playArea.width || ballX<=0)
    {
        ballDX=-ballDX;
    }
    if (ballX==rocket1X || ballX==rocket2X)
    {
        ballDx=-ballDx;
    }
    
    

}

function UpdateGame()
{
    CalculatePhysics();
    RenderObjects();
}

IntialValues();

setInterval(UpdateGame,10);
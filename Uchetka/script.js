let objectInputField = document.getElementById("objectInputField");
let priceInputField = document.getElementById("priceInputField");
let dayInputField = document.getElementById("dayInputField");
let hoursInputField = document.getElementById("hoursInputField");
let minutesInputField = document.getElementById("minutesInputField");

let PurchaseListOutputField = document.getElementById("PurchaseListOutputField");
let PurchaseList=[];

function RenderList()
{
    let htmlCode="<table border='1'>";
    htmlCode+="<tr>";
    htmlCode+="<th>Предмет</th>";
    htmlCode+="<th>Цена</th>";
    htmlCode+="<th>День</th>";
    htmlCode+="<th>Время</th>";   
    htmlCode+="</tr>";

    for(let i=0; i<PurchaseList.length;i++)
    {
        htmlCode+="<tr>";
        htmlCode+="<td>"+PurchaseList[i].Object+"</td>";
        htmlCode+="<td>"+PurchaseList[i].Price+"</td>";
        htmlCode+="<td>"+PurchaseList[i].Day+"</td>";
        htmlCode+="<td>"+PurchaseList[i].Hours+":"+PurchaseList[i].Minutes+"</td>";
        htmlCode+="</tr>";
    }
    htmlCode+="</table>";

    PurchaseListOutputField.innerHTML = htmlCode;
}
function AddPurchase()
{
    let purchase = {
        Object:objectInputField.value,
        Price:priceInputField.value,
        Day:dayInputField.value,
        Hours:hoursInputField.value,
        Minutes:minutesInputField.value
    };

    PurchaseList.push(purchase);

    objectInputField.value = "";
    priceInputField.value = "";
    dayInputField.value = "";
    hoursInputField.value = "";
    minutesInputField.value = "";

    RenderList();
}
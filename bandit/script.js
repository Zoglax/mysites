let firstNumOutField=document.getElementById("firstNumOutField");
let secondNumOutField=document.getElementById("secondNumOutField");
let thirdNumOutField=document.getElementById("thirdNumOutField");

let resultOutField=document.getElementById("resultOutField");

let compCommentaryOutputField=document.getElementById("compCommentaryOutputField");

let money=-1;

function RandomNumber(rightSide)
{
    return Math.round(Math.random()*rightSide);
}
function Roll()
{
    if (money <= 0)
    {
        let inStr = prompt("Skolko u vas deneg?");
        if(inStr==""){return;}
        money = parseInt(inStr);
        buttonRoll.disabled=false;
        buttonStart.disabled=true;
    }

    let a = RandomNumber(10);
    let b = RandomNumber(10);
    let c = RandomNumber(10);

    firstNumOutField.innerHTML=a;
    secondNumOutField.innerHTML=b;
    thirdNumOutField.innerHTML=c;

    if (a==7 && a==7 && c==7)
    {
        money += 50;
        compCommentaryOutputField.innerHTML="Viygrali 50";
    }
    else if(a==b && a==c)
    {
        money += 30;
        compCommentaryOutputField.innerHTML="Viygrali 30";
    }
    else if(a==b || a==c || c==b)
    {
        money += 20;
        compCommentaryOutputField.innerHTML="Viygrali 20";
    }
    else
    {
        money -= 10;
        compCommentaryOutputField.innerHTML="Proebali 10";
    }
    
    resultOutField.innerHTML="Tekuchiy balanss = "+money;
}